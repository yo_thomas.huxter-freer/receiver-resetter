#!/bin/sh
file=$1
while IFS= read -r line
do
	domain=$(echo $line | cut -d "," -f 1)
	echo "domain: "$domain 2>&1 | tee -a resetresponse.log
	streamid=$(echo $line | cut -d "," -f 2)
	echo "streamid: "$streamid 2>&1 | tee -a resetresponse.log
	url="http://"$domain"/csm/publish/reset/yospace01/"$streamid"?ss.exp=20301231000000"
	sig=$(echo -n $url | openssl sha1 -hmac qwelkj123x | cut -d " " -f 2)
	sigurl=$url"&ss.sig="$sig
	echo $sigurl 2>&1 | tee -a resetresponse.log
	curl -sI $sigurl 2>&1 | tee -a resetresponse.log
done < "$file"
