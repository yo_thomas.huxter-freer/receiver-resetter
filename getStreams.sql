select receiver.cdomain_name as receiver_domain, lsm_primary_broadcast_stream.cid as stream_id
from lsm_primary_broadcast_stream join adv_content_provider on adv_content_provider.cid = lsm_primary_broadcast_stream.ccontent_provider
join lsm_domain receiver on receiver.cid = lsm_primary_broadcast_stream.creceiver_domain
where ccds_user like <account name> order by adv_content_provider.ccds_user, stream_id desc;
